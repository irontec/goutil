package goutil_test

import (
	"testing"
	"time"

	"gitee.com/irontec/goutil/v1"
)

// $ go test -v -run TestV1Log test/log_test.go
func TestV1Log(t *testing.T) {
	println("This is a TEST.")

	l := goutil.LogInit()
	l.Debug("debug")
	l.Info("info")
	l.Warn("warn")
	l.Error("error")

	now := time.Now()
	l.Info()                               // 输出空行
	l.Info(123, "abc", now)                // 自动转字符串并拼接
	l.Info("%d, %s, %v,", 123, "abc", now) // 标准格式化输出
}

// log性能测试
// $ go test -v -run=^$ -bench BenchmarkV1Log test/log_test.go
func BenchmarkV1Log(b *testing.B) {
	l := goutil.LogInit()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l.Debug("debug")
		l.Info("info")
		l.Warn("warn")
		l.Error("error")
	}
}

// 标准输出性能测试, 用于对比
// $ go test -v -run=^$ -bench BenchmarkPrintln test/log_test.go
func BenchmarkPrintln(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		print("\x1B[1;90m000000_000000.000000\x1B[0m DBG \x1B[3mcommand-line-arguments_test.BenchmarkV1Log\x1B[0m \x1B[1mlog_test.go\x1B[0m:\x1B[1;36m26\x1B[0m  debug\n")
		print("\x1B[1;90m000000_000000.000000\x1B[0m \x1B[1;32mINF\x1B[0m \x1B[3mcommand-line-arguments_test.BenchmarkV1Log\x1B[0m \x1B[1mlog_test.go\x1B[0m:\x1B[1;36m27\x1B[0m  info\n")
		print("\x1B[1;90m000000_000000.000000\x1B[0m \x1B[1;33mWRN\x1B[0m \x1B[3mcommand-line-arguments_test.BenchmarkV1Log\x1B[0m \x1B[1mlog_test.go\x1B[0m:\x1B[1;36m28\x1B[0m  warn\n")
		print("\x1B[1;90m000000_000000.000000\x1B[0m \x1B[1;31mERR\x1B[0m \x1B[3mcommand-line-arguments_test.BenchmarkV1Log\x1B[0m \x1B[1mlog_test.go\x1B[0m:\x1B[1;36m29\x1B[0m  error\n")
	}
}
