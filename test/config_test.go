package goutil_test

import (
	"fmt"
	"strconv"
	"testing"

	"github.com/spf13/viper"
)

// map合并简单测试. 仅用于验证配置合并可行性
// 本测试仅处理到第二级, 未实现递归处理
// $ go test -v -run TestMapMerge test/config_test.go
func TestMapMerge(t *testing.T) {
	m1 := make(map[string]interface{})
	m1["a"] = "1"
	m1["b"] = "2"
	c := make(map[string]interface{})
	c["x"] = 1
	c["y"] = 2
	m1["c"] = c
	d := make(map[string]interface{})
	d["m"] = "o"
	m1["d"] = d
	m1["f"] = "f"
	fmt.Printf("m1: %#v\n", m1)

	m2 := make(map[string]interface{})
	m2["a"] = "3"
	m2["d"] = "3"
	e := make(map[string]interface{})
	e["x"] = 3
	e["z"] = 3
	m2["c"] = e
	m2["d"] = "d"
	f := make(map[string]interface{})
	m1["f"] = f
	f["m"] = "o"
	fmt.Printf("m2: %#v\n", m2)

	m3 := m1
	fmt.Printf("m3-pre: %#v\n", m3)
	for k, v := range m2 {
		if m, ok := m2[k].(map[string]interface{}); ok {
			n := make(map[string]interface{})
			if o, ok := m1[k].(map[string]interface{}); ok {
				n = o
			}
			for k1, v1 := range m {
				n[k1] = v1
			}
			m3[k] = n
			continue
		}
		m3[k] = v
	}
	fmt.Printf("m3-post: %#v\n", m3)
	num, _ := strconv.Atoi(m3["a"].(string))
	fmt.Printf("m3['a']: %#v\n", num)
}

// 基于viper的配置合并测试.
// viper is good. but, 不论键名还是值,
//    如果内容为y/Y/yes/Yes/YES, 则会被转换为true,
//    如果内容为n/N/no/No/NO, 则会被转换为false, 使用时需注意避坑.
// $ go test -v -run TestViperConfigMerge test/config_test.go
func TestViperConfigMerge(t *testing.T) {
	t1 := "../test/config_t1.yml"
	v1 := viper.New()
	v1.SetConfigFile(t1)
	if err := v1.ReadInConfig(); err != nil {
		panic(fmt.Errorf(t1+"read failed: %s\n", err))
	}
	fmt.Printf("read config t1: %#v\n", v1.AllSettings())

	t2 := "../test/config_t2.yml"
	v2 := viper.New()
	for k, v := range v1.AllSettings() {
		v2.SetDefault(k, v)
	}
	v2.SetConfigFile(t2)
	if err := v2.ReadInConfig(); err != nil {
		panic(fmt.Errorf(t2+"read failed: %s\n", err))
	}
	fmt.Printf("append config t2: %#v\n", v2.AllSettings())
	fmt.Printf("get test.b.q: %#v\n", v2.GetString("test.b.q"))
}
