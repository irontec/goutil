package main

import (
	"time"

	"gitee.com/irontec/goutil/v1"
	"github.com/fsnotify/fsnotify"
)

type MyCore struct {
	*goutil.GoutilCore
}

var core MyCore

func main() {
	core.GoutilCore = goutil.NewCoreDefault()
	if core.Config != nil {
		core.Log.Info("App started: %s",
			core.Config.GetString("goutil.app.name"))
		core.Config.OnConfigReload(func(e fsnotify.Event) {
			core.Log.Info("Config changed: %s [%d], reloaded!", e.Name, e.Op)
			core.Log.Info("Configs: %v", core.Config.GetAll())
		})
		core.Log.Info("Configs: %v", core.Config.GetAll())
	}
	if core.Args != nil {
		core.Log.Info("Args: %v", core.Args)
	}

	checkLogEfficient()

	// time.Sleep(60 * time.Second)
	core.Log.Info("End")
}

func checkLogEfficient() {
	// JSON: 40~60us
	// Pretty: 120~180us
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
	core.Log.Info(time.Now().Format("060102_150405.000000"))
}
